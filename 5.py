import os
import readchar

POS_X = 0
POS_Y = 0
WIDTH = 30
HEIGTH = 30
position = [0, 0]

while True:

    print(" " + "_" * WIDTH * 3 + " ")

    for coord_y in range(WIDTH):
        print("[", end="")
        for coord_x in range(HEIGTH):
            if position[POS_X] == coord_x and position[POS_Y] == coord_y:
                print(" @ ", end="")
            else:
                print("   ", end="")
        print("]")

    print(" " + "_" * WIDTH * 3 + " ")

    direction = readchar.readchar().decode()

    if direction == "w":
        position[POS_Y] -= 1

    elif direction == "s":
        position[POS_Y] += 1

    elif direction == "a":
        position[POS_X] -= 1

    elif direction == "d":
        position[POS_X] += 1

    elif direction == "x":
        break

    os.system("cls")